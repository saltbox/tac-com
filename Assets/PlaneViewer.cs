﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaneViewer : MonoBehaviour {
	public int planeIndex = 0;
	public bool friendlyPlane;
	Plane p;
	Text text;
	public bool started = false;
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {
		if (!started) {
			if (friendlyPlane) {
				p = PlaneManager.friendlyPlanes[planeIndex];
			} else {
				p = PlaneManager.hostilePlanes[planeIndex];
			}
			text = GetComponent<Text>();
			started = true;
			text.color = Color.black;
		}

		string targetnum = "N/A";
		if (p.target != null) {
			targetnum = p.target.num.ToString();
		}

		string pursuers = "N/A";
		if (p.pursuerPrimary != null) {
			pursuers = p.pursuerPrimary.num.ToString();
		}
		if (p.pursuerSecondary != null) {
			if (pursuers != "N/A") {
				pursuers += " " + p.pursuerSecondary.num.ToString();
			} else {
				pursuers = p.pursuerSecondary.num.ToString();
			}
		}

		text.text = "Num: " + p.num.ToString() + "\nPursuers: \n" + pursuers + "\nPursuit: \n" + (Mathf.Round(p.pursuit * 10) / 10) + "\nComp: \n" + (Mathf.Round(p.pilot.composure * 10) / 10) + "\nTarget: " + targetnum + "\nDowned: " + p.downed;
		if (p.downed) {
			text.color = Color.red;
		}
	}
}
