﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextClipper : MonoBehaviour {
	Text text;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (text.text.Length > 800) {
			text.text = text.text.Substring(0, 700);
		}
	}
}
