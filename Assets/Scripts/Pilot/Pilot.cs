﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class Pilot {
    public static Message[,] chatter;

    public float energy = 0, composure = 1, experience = 0;
	public Personality personality;
	// Use this for initialization
	public Pilot() {
		if (chatter == null)
		{
			chatter = new Message[Enum.GetNames(typeof(MessageType)).Length, Enum.GetNames(typeof(Personality)).Length];
			PopulateChatter();
		}
		personality = (Personality)UnityEngine.Random.Range(0, 3);
	}

	public Pilot(Personality p) {
		if (chatter == null)
		{
			chatter = new Message[Enum.GetNames(typeof(MessageType)).Length, Enum.GetNames(typeof(Personality)).Length];
			PopulateChatter();
		}
		personality = p;
	}

	public Message SayMessageOfType(MessageType type) {
		SayMessage(chatter[(int)type, (int)personality]);
        return chatter[(int)type, (int)personality];
	}

	public void SayMessage(Message message) {
		GameObject.Find("MessageLog").GetComponent<Text>().text += message.GetText() + "\n";
	}

	public void RespondToMessage(Message message) {
		SayMessage(message.responses[(int)personality][0]);
	}

    static void PopulateChatter()
    {
		chatter[(int)MessageType.MissileAway, (int)Personality.Newbie] = new Message(MessageType.MissileAway, Personality.Newbie, new string[] {"I'M FIRING", "FIRING A MISSILE", "THIS BETTER HIT"});
        chatter[(int)MessageType.MissileAway, (int)Personality.Newbie].responses = new Message[3][];
        chatter[(int)MessageType.MissileAway, (int)Personality.Newbie].responses[1] = new Message[1];
        chatter[(int)MessageType.MissileAway, (int)Personality.Newbie].responses[(int)Personality.Maverick][0] = new Message(MessageType.Response, Personality.Maverick, new string[] { "YOU TOOK THE LENSCAP OFF RIGHT" });
		chatter[(int)MessageType.MissileAway, (int)Personality.Maverick] = new Message(MessageType.MissileAway, Personality.Maverick, new string[] {"FOX ONE", "FOX ONE FOX ONE", "MISSILE AWAY"});
		chatter[(int)MessageType.MissileAway, (int)Personality.Ghost] = new Message(MessageType.MissileAway, Personality.Ghost, new string[] {"MISSILE", "FIRING", "PAYLOAD"});

        chatter[(int)MessageType.BogeyLocked, (int)Personality.Newbie] = new Message(MessageType.BogeyLocked, Personality.Newbie, new string[] {"I GOT A LOCK", "I'M LOCKED ONTO THEM"});
        chatter[(int)MessageType.BogeyLocked, (int)Personality.Maverick] = new Message(MessageType.BogeyLocked, Personality.Maverick, new string[] {"THEY'RE IN MY SIGHTS", "I'M READY TO FIRE"});
        chatter[(int)MessageType.BogeyLocked, (int)Personality.Ghost] = new Message(MessageType.BogeyLocked, Personality.Ghost, new string[] {"LOCKED", "LOCK"});
    }
}

public class Message
{
    public Personality personality;
	public MessageType messageType;
    public string[] texts;
	public Message[][] responses;

    public Message(MessageType t, Personality p, string[] texts)
    {
        personality = p;
		messageType = t;
        this.texts = texts;
    }

	public string GetText() {
		return texts[UnityEngine.Random.Range(0, texts.Length)];
	}

	public void PopulateResponses(Message[][] responses) {
		this.responses = responses;
	}
}

public enum Personality
{
    Newbie,
    Maverick,
    Ghost,
}

public enum MessageType
{
    MissileAway,
    BogeyLocked,
    Response
}