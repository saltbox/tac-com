﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlaneManager : MonoBehaviour {
	public static List<Plane> friendlyPlanes, hostilePlanes;
	public int seed;
	public bool useSeed;
	List<Plane> planes;
	int[] indexes;
	public int friendlyWinCount, hostileWinCount;
	// Use this for initialization
	void Start () {
		if (useSeed) {
			Random.seed = seed;
		}
		friendlyPlanes = new List<Plane>();
		hostilePlanes = new List<Plane>();

		for(int i = 0; i < 6; i++) {
			friendlyPlanes.Add(new Plane(friendlyPlanes, hostilePlanes, i));
			hostilePlanes.Add(new Plane(hostilePlanes, friendlyPlanes, i + 6));
		}

		planes = new List<Plane>();
		planes.AddRange(friendlyPlanes);
		planes.AddRange(hostilePlanes);
		indexes = new int[12];
		for(int i = 0; i < 12; i++) {
			indexes[i] = i;
		}
		Shuffle(ref indexes);

		foreach(PlaneViewer viewer in GameObject.FindObjectsOfType<PlaneViewer>()) {
			viewer.started = false;
		}
	}
	

	// Update is called once per frame
	void FixedUpdate () {
		foreach(int i in indexes){
			planes[i].Update();
		}
		if (checkCleared(friendlyPlanes)) {
			Reset();
			hostileWinCount++;
		}
		if (checkCleared(hostilePlanes)) {
			Reset();
			friendlyWinCount++;
		}

	}

	bool checkCleared(List<Plane> l) {
		foreach(Plane p in l) {
			if (!p.downed) {
				return false;
			}
		}
		return true;
	}

	void Reset() {
		Start();
	}

	void Shuffle(ref int[] indexes) {
		int n = indexes.Length;
		while (n > 1)
		{
			n--;
			int k = Random.Range(0, n + 1);
			int value = indexes[k];
			indexes[k] = indexes[n];
			indexes[n] = value;
		}
	}

}
