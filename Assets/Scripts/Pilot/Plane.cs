﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Plane {
    public Pilot pilot;
    public string name;
    public float maneuverability = 1, topspeed;
    public Plane target, pursuerPrimary, pursuerSecondary, partner;
    public float pursuit = 0;
	public bool downed = false;
	public int num;

    public int missiles = 9;
    public float fuel;  
    public float wings = 1, cockpit = 1, fuselage = 1, engine = 1;

    public float evadeMultiplier = 0.15f, pursuitMultiplier = 0.2f;

	List<Plane> hostilePlanes;
	Text combatLogText;

	public Plane (List<Plane> friendlyPlanes, List<Plane> hostilePlanes, int num) {
		//this.friendlyPlanes = friendlyPlanes;
		this.hostilePlanes = hostilePlanes;
		pilot = new Pilot(Personality.Maverick);
		combatLogText = GameObject.Find("CombatLog").GetComponent<Text>();
		this.num = num;
	}

    public void EvadeManeuver()
    {	
		if (pursuerPrimary != null) {
        	pursuerPrimary.pursuit -= maneuverability * evadeMultiplier;
			pursuerPrimary.pursuit = Mathf.Clamp01(pursuerPrimary.pursuit);
		}
		if (pursuerSecondary != null) {
        	pursuerSecondary.pursuit -= maneuverability * evadeMultiplier * 0.8f;
			pursuerSecondary.pursuit = Mathf.Clamp01(pursuerSecondary.pursuit);
		}
#if DEBUG
		combatLogText.text = combatLogText.text.Insert(0,num + ": Evade\n");
#endif
    }

    public void PursuitManeuver()
    {
        pursuit += maneuverability * pursuitMultiplier * Random.Range(0.8f, 1.2f);
#if DEBUG
		combatLogText.text = combatLogText.text.Insert(0,num + ": Pursuit\n");
#endif
    }

	public void Update() {
		if (pursuerPrimary != null && pursuerPrimary.downed) {
			pursuerPrimary = null;
		}
		if (pursuerSecondary != null && pursuerSecondary.downed) {
			pursuerSecondary = null;
		}
		if (wings <= 0 || engine <= 0 || cockpit <= 0|| fuselage <= 0) {
			downed = true;
			target = null;
			if (pursuerPrimary != null)
				pursuerPrimary.target = null;
			if (pursuerSecondary != null)
				pursuerSecondary.target = null;
			pursuerPrimary = null;
			pursuerSecondary = null;
		}
		if (target == null && !downed) {
			GetTarget();
		} else if (!downed){
			if (target != null && target.pursuerSecondary != null && target.pursuerSecondary == this && target.pursuerPrimary == null) {
				target.pursuerPrimary = this;
				target.pursuerSecondary = null;
			}
			if (pursuerPrimary == null && pursuerSecondary == null && pursuit < 1) {
				PursuitManeuver();
			} else if (pursuerPrimary != null && pursuerPrimary.pursuit < 0.5f && (pursuerSecondary == null || pursuerSecondary.pursuit < 0.1f) && pursuit < 1) {
				PursuitManeuver();
			} else {
				EvadeManeuver();
			}
			if (target != null && pursuit > 0.6f && missiles > 0) {
				Missile();
			}
			if (target != null && pursuit > 0.85f) {
				Guns();
			}
		}
	}

	void GetTarget() {
		if (partner != null && !partner.downed && partner.target != null) {
			if (partner.target.pursuerSecondary == null) {
				target = partner.target;
				target.pursuerSecondary = this;
				return;
			} else if (partner.target.pursuerPrimary == null) {
				target = partner.target;
				target.pursuerPrimary = this;
				return;
			}
		} else {
			foreach(Plane p in hostilePlanes) {
				if (!p.downed) {
					if (p.pursuerPrimary == null) {
						target = p;
						target.pursuerPrimary = this;
						return;
					} else if (p.pursuerSecondary == null) {
						target = p;
						target.pursuerSecondary = this;
						return;
					}
				}
			}
		}
	}

    public bool Missile()
    {
        missiles--;
#if DEBUG
		combatLogText.text = combatLogText.text.Insert(0,num + ": Missile " + target.num + "\n");
#endif
		target.pilot.composure -= Random.Range(0f, 0.1f);
        if (Random.Range(0, 1) + pursuit > 0.7 + target.pilot.composure * 0.2)
        {
			#if DEBUG
			combatLogText.text = combatLogText.text.Insert(0,num + ": Missile Hit\n");
			#endif
            switch (Random.Range(0, 4))
            {
                case 0:
                    target.wings -= Random.Range(1.2f, 1.6f);
                    break;
                case 1:
                    target.cockpit -= Random.Range(1.2f, 1.6f);
                    break;
                case 2:
                    target.fuselage -= Random.Range(1.2f, 1.6f);
                    break;
                case 3:
                    target.engine -= Random.Range(1.2f, 1.6f);
                    break;
            }
            return true;
        }
        return false;
    }

    public void Guns()
    {
#if DEBUG
		combatLogText.text = combatLogText.text	.Insert(0,num + ": Gun " + target.num + "\n");
#endif
		target.pilot.composure -= Random.Range(0f, 0.1f);
        if (Random.Range(0, 1) + pursuit > 1.6 + target.pilot.composure * 0.2)
        {
			#if DEBUG
			combatLogText.text = combatLogText.text.Insert(0,num + ": Gun Hit\n");
			#endif
            if (Random.Range(0, 2) == 1)
            {
                target.wings -= Random.Range(0.2f, 0.6f);
            }
            if (Random.Range(0, 2) == 1)
            {
                target.cockpit -= Random.Range(0.2f, 0.6f);
            }
            if (Random.Range(0, 2) == 1)
            {
                target.fuselage -= Random.Range(0.2f, 0.6f);
            }
            if (Random.Range(0, 2) == 1)
            {
                target.engine -= Random.Range(0.2f, 0.6f);
            }
        }
    }
}