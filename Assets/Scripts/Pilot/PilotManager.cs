﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PilotManager : MonoBehaviour {
	public List<Pilot> pilots;
	Pilot activePilot;
	// Use this for initialization
	void Start () {
		pilots = new List<Pilot>();
		pilots.Add(new Pilot(Personality.Newbie));
		pilots.Add(new Pilot(Personality.Maverick));
		pilots.Add(new Pilot(Personality.Ghost));
		activePilot = pilots[0];
	}

	public void SetActivePilot(int i) {
		activePilot = pilots[i];
	}

    public void PilotSendMessage(Pilot p, MessageType type)
    {
        Message saidMessage = p.SayMessageOfType(type);
        foreach (Pilot other in pilots)
        {
            if (Random.Range(0, 5) == 4)
            {
                if (saidMessage.responses[(int)other.personality].Length > 0)
                {
                    other.SayMessage(saidMessage.responses[(int)other.personality][Random.Range(0, saidMessage.responses[(int)other.personality].Length)]);
                }
            }
        }
    }
	
	public void SendMissileMessage() {
        PilotSendMessage(activePilot, MessageType.MissileAway);
	}

	public void SendLockMessage() {
        PilotSendMessage(activePilot, MessageType.BogeyLocked);
	}
}
